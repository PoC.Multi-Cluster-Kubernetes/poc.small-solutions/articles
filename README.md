# Articles



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [GitOps with ArgoCD, EKS and GitLab CI using Terraform](https://medium.com/@calvineotieno010/gitops-with-argocd-eks-and-gitlab-ci-using-terraform-2a3c094b4ea3) files

- [ ] [Kubernetes Multi-cluster Management – Part 1](https://blogs.perficient.com/2022/11/16/kubernetes-multi-cluster-management-series-part-1/) files
Multi-cluster management for Kubernetes with Cluster API and Argo CD
https://aws.amazon.com/pt/blogs/containers/multi-cluster-management-for-kubernetes-with-cluster-api-and-argo-cd/


Run an active-active multi-region Kubernetes application with AppMesh and EKS
https://aws.amazon.com/pt/blogs/containers/run-an-active-active-multi-region-kubernetes-application-with-appmesh-and-eks/

Operating a multi-regional stateless application using Amazon EKS
https://aws.amazon.com/blogs/containers/operating-a-multi-regional-stateless-application-using-amazon-eks/


- [ ] [Exposing Kubernetes Applications, Part 1: Service and Ingress Resources](https://aws.amazon.com/blogs/containers/exposing-kubernetes-applications-part-1-service-and-ingress-resources/)

AWS API Gateway - EC2 Integration (Console + Terraform | Backend | Endpoint | HTTP | Node JS)
https://www.youtube.com/watch?v=XhS2JbPg8jA

Native EKS Ingress: AWS Load Balancer Controller & 5 Examples (Terraform, TLS, IngressGroup, IP)
https://www.youtube.com/watch?v=ZfjpWOC5eoE

https://gist.github.com/jakubhajek/2f26599415dd84a6fd2c23b56275dc03
https://github.com/traefik-workshops/traefik-workshop


Using a Network Load Balancer with the NGINX Ingress Controller on Amazon EKS
https://aws.amazon.com/blogs/opensource/network-load-balancer-nginx-ingress-controller-eks/


How to manage access and EKS permissions?
https://www.padok.fr/en/blog/aws-eks-cluster


Amazon EKS and ArgoCD
https://awstip.com/amazon-eks-and-argocd-bb954c980cef

Routing your microservice architectures with Traefik (Jakub Hajek, CNP#10)
https://www.youtube.com/watch?v=lWRpaBXkJ98


Architecting Kubernetes Clusters for High-Traffic Websites
https://www.youtube.com/watch?v=t_CiHcKE-dc&t=82s


Installing Harbor on Kubernetes with Project Contour, Cert Manager, and Let’s Encrypt
https://tanzu.vmware.com/developer/guides/harbor-gs/


NGINX Ingress Controller for Amazon EKS — Frequently Used Annotations
https://www.qloudx.com/nginx-ingress-controller-for-amazon-eks-frequently-used-annotations/

Advanced API Routing in EKS with Traefik, AWS LoadBalancer Controller and External-DNS
https://www.revolgy.com/insights/blog/advanced-api-routing-in-eks-with-traefik-aws-loadbalancer-controller-and-external-dns


API Gateway Pattern
https://medium.com/design-microservices-architecture-with-patterns/api-gateway-pattern-8ed0ddfce9df


INGRESS CONTROLLER AND EXTERNAL DNS WITH ROUTE53 ON EKS
https://nahuelhernandez.com/blog/ingress_and_external_dns_with_route53_on_eks/#:~:text=In%20short%2C%20external%20DNS%20is,for%20that%20resource%20in%20Route53


Blue/Green or Canary Amazon EKS clusters migration for stateless ArgoCD workloads
https://aws.amazon.com/blogs/containers/blue-green-or-canary-amazon-eks-clusters-migration-for-stateless-argocd-workloads/



## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/PoC.Multi-Cluster-Kubernetes/poc.small-solutions/articles/-/settings/integrations)

